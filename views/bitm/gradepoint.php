<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mark Collection Form</title>
    <style>
        div{
            width: 30%;
            margin: 20px auto;
            display: block;
        }
        input{
            float: right;
        }
    </style>
</head>
<body>
<div>
<form action="point.php" method="post">
    Please Enter Your Name:
    <input type="text" name="studentname" placeholder="Name">
    <br>
    Please Enter Your Roll Number:
    <input type="text" name="studentroll" placeholder="Roll">
    <br>
    Subject Entry:
    <br>
    Bengali:
    <input type="number" step="any" name="subBen">
    <br>
    English:
    <input type="number" step="any" name="subEng">
    <br>
    Mathematics:
    <input type="number" step="any" name="subMath">
    <br>
    <input type="submit" value="SUBMIT">
</div>
</form>
</body>
</html>