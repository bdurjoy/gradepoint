<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Student Information and Marks Collection Form</title>

    <style>
        div{
            width: 60%;
            margin: 20px auto;
            display: block;
        }
        input{
            float: right;
        }
    </style>


</head>
<body>

<div class="container">



    <div class="headline">

        <h1> Student Information and Marks Collection Form</h1>

    </div>

    <form action="process.php" method="post">

        <div class="studentInfo">
                Name:
              <input type="text" name="name" > <br>

              Student ID:
              <input type="text" name="studentID" >  <br>

        </div>


        <div class="marksInfo">

             Mark Bangla:
            <input type="number" step="any" name="markBangla" > <br>

            Mark English:
            <input type="number" step="any" name="markEnglish" >  <br>

            Mark Math:
            <input type="number" step="any" name="markMath" > <br>


        </div>


        <div class="submitForm">

            <input style="width: 100%" type="submit">

        </div>

    </form>

</div>

</body>
</html>