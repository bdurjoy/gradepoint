<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/21/2017
 * Time: 2:00 PM
*/

namespace App;


class student
{
    private $studentname;
    private $studentroll;


    public function setStudentname($studentname)
    {
        $this->studentname = $studentname;
    }

    public function getStudentname()
    {
        return $this->studentname;
    }

    public function setStudentroll($studentroll)
    {
        $this->studentroll = $studentroll;
    }

    public function getStudentroll()
    {
        return $this->studentroll;
    }

}