<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/21/2017
 * Time: 2:19 PM
 */

namespace App;


class course
{
    private $subBen;
    private $gradesubBen;

    private $subEng;
    private $gradesubEng;

    private $subMath;
    private $gradesubMath;

    private $result;

    public function setSubBen($subBen)
    {
        $this->subBen = $subBen;
    }

    public function getSubBen()
    {
        return $this->subBen;
    }

    public function setSubEng($subEng)
    {
        $this->subEng = $subEng;
    }

    public function getSubEng()
    {
        return $this->subEng;
    }

    public function setSubMath($subMath)
    {
        $this->subMath = $subMath;
    }

    public function getSubMath()
    {
        return $this->subMath;
    }

    public function getGradesubBen()
    {
        $this->result = $this->mark2grade($this->subBen);
    }

    public function setGradesubBen()
    {
        return $this->gradesubBen;
    }

    public function getGradesubEng()
    {
        $this->result = $this->mark2grade($this->subEng);
    }

    public function setGradesubEng()
    {
        return $this->gradesubEng;
    }

    public function getGradesubMath()
    {
        $this->result = $this->mark2grade($this->subMath);
    }

    public function setGradesubMath()
    {
        return $this->gradesubMath;
    }


    public function setResult($result)
    {
        $this->result = $result;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function mark2grade($mark){
        switch($mark){
            case ($mark>79):
                $grade= "A+";
                break;

        }
        return $grade;
    }

}